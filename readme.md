Name : Payut Tanyluck Id : 5680913
Name : Pongwasin Wintakorn Id : 5680975

Brief Summary : For this project, we tried to do a map which include extraordinary
                data such as sea surface temperature, humidity, satellite and radar.

List of APIs and libraries used : We use leaflet api to retrieve the map and aeris api
                                  to retrieve special data from its server which are
                                  sea surface temperature, humidity, satellite and radar.


List of known bugs : We tried to fix the api since the api only allow us to collect the data
                     from only us region, however, we did our best to find the solution
                     to get the global data but it does not work out.